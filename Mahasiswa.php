<?php
    include 'DbConn.php';
	include 'Users.php';

    class Mahasiswa {
        private $table = 'mahasiswa';

        public function __construct($nim = null, $nama = null, $jenis_kelamin = null, $foto = null, $username = null, $password = null) {
            $conn = new DbConn();

            $this->conn = $conn->getConn();

            $this->nim = $nim;
            $this->nama = $nama;
            $this->jenis_kelamin = $jenis_kelamin;
            $this->foto = $foto;
			$this->username = $username;
			$this->password = md5($password);
        }

        public function insert() {
			$user = new Users($this->username, $this->password);
			
			$user->insert();
            
            $sql = 'INSERT INTO '.$this->table.' (nim, nama, jenis_kelamin, foto, id_users) 
            VALUES ("'.$this->nim.'", "'.$this->nama.'", "'.$this->jenis_kelamin.'", "'.$this->foto.'", "'.$user->last_id().'")';

            return mysqli_query($this->conn, $sql);
        }

        public function read() {
            $sql = 'SELECT * FROM '.$this->table;

            $result = mysqli_query($this->conn, $sql);

            $data = [];

            while($row = mysqli_fetch_object($result)) {
                $data[] = $row;
            }

            return $data;
        }
		
		public function read_spec_data($nim){
			$sql = 'SELECT * FROM '.$this->table.' WHERE nim="'.$nim.'"';
            $result = mysqli_query($this->conn, $sql);
            $data = mysqli_fetch_object($result);
			return $data;
		}
		
        public function update() {
			$sql ="UPDATE `mahasiswa` SET `nama` = '".$this->nama."', 
			              `jenis_kelamin` = '".$this->jenis_kelamin."', 
						  `foto` = '".$this->foto."'
						  WHERE `mahasiswa`.`nim` = '".$this->nim."'";
			return mysqli_query($this->conn, $sql);	 //agar aksi dapat berdampak maka harus di return		  
		}

        public function delete($nim) {
			$mhs = $this->read_spec_data($nim);
			
			$user = new Users();
			
			$user->delete($mhs->id_users);
			
            $sql = 'DELETE FROM '.$this->table.' WHERE nim="'.$nim.'"';

            return mysqli_query($this->conn, $sql);
        }
    }
?>